import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import '../../styles/App.scss';
import logo from '../../images/castleLogo.svg';

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      successMsg: null,
      errorMsg: null,
    }

    this.handleSubmit = this.handleSubmit.bind(this);
  }



  handleSubmit () {
    axios.post('/api/signup', {
      email: this.state.email,
      password: this.state.password,
    })
      .then(res => {
        if (res.data.response.local.email !== '') {
          this.setState({ successMsg: 'Success! Please login.' });
        } else {
          this.setState({ errorMsg: 'Oops. Something went wrong.' });
        }
      })
  }


  render () {
    const state = this.state;
    const hasSignUpError = state.errorMsg;
    const successfulLogin = state.successMsg;

    return (
      <div className="form-wrapper">
        <div className="ui container login-form">
          <header>
            <img src={logo} alt="" />
          </header>

          <React.Fragment>
            {
              hasSignUpError &&
              <h3>{hasSignUpError}</h3>
            }

            {
              successfulLogin &&
              <React.Fragment>
                <h3>{successfulLogin}</h3>
                <Link to="/">
                  <button className="ui primary button">Sign in</button>
                </Link>
              </React.Fragment>
            }

            {
              !hasSignUpError && !successfulLogin &&
              <React.Fragment>
                <h3>Please Sign up</h3>
                <div className="ui input large">
                  <input
                    type="text"
                    placeholder="Email address"
                    onChange={(e) => this.setState({ email: e.target.value })} />
                </div>
                <div className="ui input large">
                  <input
                    type="password"
                    placeholder="Password"
                    onChange={(e) => this.setState({ password: e.target.value })} />
                </div>
                <button
                  onClick={this.handleSubmit}
                  className="ui primary button">Sign Up</button>
              </React.Fragment>
            }
          </React.Fragment>
        </div>
        <footer className="copyright">© 2019</footer>
      </div>
    );
  }
}

export default SignUp;