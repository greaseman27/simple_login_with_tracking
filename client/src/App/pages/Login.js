import React, { Component } from 'react';
import Cookies from 'js-cookie';
import axios from 'axios';

import '../../styles/App.scss';
import logo from '../../images/castleLogo.svg';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      successMsg: null,
      errorMsg: null,
      userId: null,
      activeSession: false || this.checkActiveSession(),
    }

    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.checkActiveSession = this.checkActiveSession.bind(this);
    this.setActiveSession = this.setActiveSession.bind(this);
  }

  componentDidMount () {
    this.checkActiveSession();
  }

  handleLogin () {
    console.log('castle client ID:', window._castle('getClientId'));
    const clientId = window._castle('getClientId');
    axios.post('/api/login', {
      email: this.state.email,
      password: this.state.password,
      clientId: clientId
    })
      .then(res => {
        if (res.data.user) {
          this.setActiveSession(res.data.user);
        } else {
          this.setState({ errorMsg: 'User not found.' })
        }
      })
  }

  handleLogout () {
    document.cookie = 'activeSession=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    this.setState({
      activeSession: false,
      email: null,
      password: null,
    }, () => {
      axios.get('/api/logout')
        .then(() => window._castle('reset'));
    });
  }

  setActiveSession (user) {
    Cookies.set('activeSession', `${this.state.email}`);
    this.setState({
      successMsg: 'Welcome',
      activeSession: true,
      errorMsg: null,
      userId: user._id
    }, window._castle('identify', user._id));
  }

  checkActiveSession (name = 'activeSession') {
    const loggedInUser = Cookies.get(name);
    if (loggedInUser) {
      this.setState({
        email: loggedInUser,
        successMsg: 'Welcome',
        activeSession: true,
        errorMsg: null,
      });
    }
  }



  render () {
    const state = this.state;
    const isLoggedIn = state.activeSession;
    const hasLoginError = state.errorMsg;


    return (
      <div className="form-wrapper">
        <div className="ui container login-form">
          <header>
            <img src={logo} alt="" />
          </header>
          {
            isLoggedIn &&
            <h3>{state.successMsg}, {state.email}!</h3>
          }

          {
            hasLoginError &&
            <h3>{this.state.errorMsg}</h3>
          }

          {
            !isLoggedIn &&
            <React.Fragment>
              {
                !hasLoginError &&
                <h3>Please log in</h3>
              }
              <div className="ui input large">
                <input
                  type="text"
                  placeholder="Email address"
                  onChange={(e) => this.setState({ email: e.target.value })} />
              </div>
              <div className="ui input large">
                <input
                  type="password"
                  placeholder="Password"
                  onChange={(e) => this.setState({ password: e.target.value })} />
              </div>
            </React.Fragment>
          }
          {
            !isLoggedIn &&
            <button
              onClick={this.handleLogin}
              className="ui primary button">Sign In</button>
          }

          {
            isLoggedIn &&
            <button
              onClick={this.handleLogout}
              className="ui primary button">Sign Out</button>
          }

        </div>
        <footer className="copyright">© 2019</footer>
      </div>
    );
  }
}

export default Login;