import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import '../styles/App.scss';
import Login from './pages/Login';
import SignUp from './pages/SignUp';

class App extends Component {
  render () {
    const App = () => (
      <React.Fragment>
        <Helmet>
          <script src="https://d2t77mnxyo7adj.cloudfront.net/v1/c.js?469977721162171"></script>
        </Helmet>
        <Switch>
          <Route exact path='/' component={Login} />
          <Route path='/signup' component={SignUp} />
        </Switch>
      </React.Fragment>
    )
    return (
      <App />
    );
  }
}

export default App;