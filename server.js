const express = require('express');
const app = express();
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');

const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const path = require('path');

const configDB = require('./config/database');

//* configuration
mongoose.connect(configDB.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });

require('./config/passport')(passport);

//* Express application
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'ejs');

//* Passport requirements
app.use(session({ secret: 'useTheForceLuke' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


//* Serve static files from our React App.
app.use(express.static(path.join(__dirname, 'client/build')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

// import { Castle, EVENTS } from '@castleio/sdk';
const CastleObj = require('@castleio/sdk');
const EVENTS = CastleObj.EVENTS;
const castle = new CastleObj.Castle({ apiSecret: '9CN9VykX7Qhrp1yp9TdCizYSrEb1nrtm' });

//* routes config
require('./app/routes.js')(app, passport, castle, EVENTS); // load our routes and pass in our app and fully configured passport

//* Go time!
const port = process.env.PORT || 8080;
app.listen(port);
console.log('App is running on port: ' + port);