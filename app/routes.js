module.exports = (app, passport, castle, EVENTS) => {

  app.post('/api/signup', (req, res, next) => {
    passport.authenticate('local-signup', (err, data, info) => {
      if (err) { return next(err) }
      return res.status(200).json({ response: data });
    })(req, res, next);
    console.log('signup req', req);
    console.log('signup res', res);
  })


  app.post('/api/login', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
      console.log('User:', user);
      if (err) {
        return next(err);
      }

      if (user === 'badPassword') {
        badPassword(req, user, castle, EVENTS);
        return res.status(200).json({ user: null, error: 'Invalid password' });
      }


      if (!user) {
        return res.status(200).json({ user: null, error: 'User not found' });
      }

      req.logIn(user, async function (err) {
        if (err) { return next(err); }
        loginSuccess(req, user, castle, EVENTS);
        return res.status(200).json({ user: req.user });
      });
    })(req, res, next);
    console.log(req);
    console.log(res);
  });


  app.get('/api/logout', (req, res) => {
    req.logout();
    res.status(200).json({ action: 'User logged out' });
  });

};

//* Middleware functions ========================= */

const badPassword = async (req, user, castle, EVENTS) => {
  let response;
  try {
    response = await castle.track({
      event: EVENTS.LOGIN_FAILED,
      user_id: user.id, // => null
      user_traits: {
        email: user.email,
        registered_at: user.registered_at,
      },
      context: {
        ip: req.ip,
        client_id: req.cookies['__cid'],
        headers: req.headers,
      },
    });
  } catch (e) {
    console.error(e);
  }

  console.log(response);
}

const loginSuccess = async (req, user, castle, EVENTS) => {
  let response;
  try {
    response = await castle.authenticate({
      event: EVENTS.LOGIN_SUCCEEDED,
      user_id: user.id,
      user_traits: {
        email: user.email,
        registered_at: user.registered_at,
      },
      context: {
        ip: req.ip,
        client_id: req.cookies['__cid'],
        headers: req.headers,
      },
    });
  } catch (e) {
    console.error(e);
  }

  console.log(response);
}

//* route middleware to make sure a user is logged in
const isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated())
    return next();
  res.redirect('/');
}