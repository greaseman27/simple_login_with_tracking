## This project was built with NodeJS, PassportJS, and ReactJS.
1. Clone this repository.
2. `cd into the 'client' directory`.
3. `npm install` on your command line.
4. Once your build has completed, cd back into the parent directory `cd ..`
5. Type `npm install`
6. Once your dependencies for the parent directory have finished installing, type `npm start`.
7. You should now be able to access the application running on `localhost:8080`.
9. Navigate to `localhost:8080/signup`
10. Signup using an email and password (there is no validation on either of these fields for demo purposes).
11. Use the button on current page to navigate to the login page. Or go to `localhost:8080` in your url bar.
12. Login using the username and password you just signed up with.